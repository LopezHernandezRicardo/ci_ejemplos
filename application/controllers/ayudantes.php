<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ayudantes extends CI_Controller
{
  private $datos;

  public function __construct()
  {
    parent::__construct();
    $datos = array();
  }
  
  public function index()
  {
    /* Haciendo uso de la función redirect() del Helper URL
     * podemos redireccionar al cliente cuando solicite:
     * /index.php/ayudantes o bien a
     * /index.php/ayudantes/index
     *
     */
    redirect("ayudantes/url");
  }

  public function url()
  {
    $this->load->view('ayudantes/vista_url');
  }
  
  public function formulario()
  {
    $this->load->view('ayudantes/vista_formulario');
  }

  public function formulario_post()
  { 
    $this->load->view('ayudantes/vista_formulario_post');
  }
}

