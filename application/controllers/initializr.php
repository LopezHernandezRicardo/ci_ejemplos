<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Initializr extends CI_Controller
{
  private $datos;

  public function __construct()
  {
    parent::__construct();
    $this->datos = array();
  }
  
  public function index()
  {
    /* Está acción carga una vista con el contenido del archivo
     *   /recursos/initializr/index.html
     * que es el template o plantilla que ofrece Initializr.
     *
     * En está vista se hace uso de la función base_url() para
     * generar los vínculos correctos para los recursos (js y css)
     * que ofrece Initializr.
     */
    $this->load->view('initializr/vista_index.php');
  }
  
  public function ingresar()
  {
    /* A diferencia de la acción index() está acción separa
     * las vistas en:
     */
    $this->datos['cuerpo_pagina'] = 'initializr/vista_ingresar';
    $this->load->view('initializr/vista_base.php', $this->datos);
  }
  
}
