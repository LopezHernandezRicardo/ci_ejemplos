<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Helper Form</title>
</head>

<body>
<h1>Helper Form</h1>

<h2>Formulario de ingreso</h2>
<?php

/* Los saltos de línea (\n) son para
 * visualizar mejor el código HTML generado
 */

  echo form_open("ayudantes/formulario_post");
  echo "\n";
  echo form_fieldset("Ingreso");
  echo "\n";
  echo form_label("Usuario:", "usuario");
  echo "\n";
  echo form_input("usuario", "");
  echo "\n";
  echo form_label("Contraseña:", "password");
  echo "\n";
  echo form_password("password", "");
  echo "\n";
  echo form_submit("boton_ingresar", "Ingresar");
  echo "\n";
  echo form_fieldset_close();
  echo "\n";
  echo form_close();
?>


<hr />
<h2>Formulario para un nuevo libro</h2>


<?php
  echo form_open("ayudantes/formulario_post");
  echo "\n";
  echo form_fieldset("Datos de nuevo libro");
  echo "\n";
  echo form_label("ISBN:", "isbn");
  echo "\n";
  echo form_input("isbn", "");
  echo "\n";
  echo form_label("Titulo:", "titulo");
  echo "\n";
  echo form_password("titulo", "");
  echo "\n";

    echo "<!-- Inicio de Autores -->";
    echo "\n";
    echo form_fieldset("Autor/es");
    echo "\n";
    /* El uso de parrafos (<p>) es un aspecto visual */
    echo "<p>";
    echo "\n";
    echo form_label("Nombre de Autor:", "autor[]");
    echo "\n";
    echo form_input("autor[]", "");
    echo "\n";
    echo "</p>";
    echo "\n";
    echo "<p>";
    echo "\n";
    echo form_label("Nombre de Autor:", "autor[]");
    echo "\n";
    echo form_input("autor[]", "");
    echo "\n";
    echo "</p>";
    echo "\n";
    echo "<p>";
    echo "\n";
    echo form_label("Nombre de Autor:", "autor[]");
    echo "\n";
    echo form_input("autor[]", "");
    echo "\n";
    echo "</p>";
    echo "\n";
    echo form_fieldset_close();
    echo "\n";
    echo "<!-- Fin de Autores -->";
    echo "\n";

  echo form_submit("boton_crear", "Crear");
  echo "\n";
  echo form_fieldset_close();
  echo "\n";
  echo form_close();
?>


</body>
</html>
