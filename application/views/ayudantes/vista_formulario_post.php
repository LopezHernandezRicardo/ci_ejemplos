<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Helper Form</title>
</head>

<body>

<h1>Helper Form</h1>

<p>
<?php

/* Se hace uso de la clase Input de CodeIgniter
 *
 * Seguridad - pág 80 de la Guía de Usuario
 * Clase Input - pág 196 de la Guía de Usuario
 *
 */

  $post = $this->input->post(NULL, TRUE);
  if (!empty($post['boton_ingresar'])) {
    echo "Clic en botón ingresar";
  } else {
    echo "Clic en botón crear";
  }
?>
</p>

<h1>Valores subidos mediante POST</h1>

<pre>
<?php
  var_dump($post);
?>
</pre>

<?php
  echo anchor("ayudantes/formulario", "Regresar");
?>
