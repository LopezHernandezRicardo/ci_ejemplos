<?php $this->load->view('initializr/vista_cabecera.php'); ?>

<?php $this->load->view('initializr/vista_menu.php'); ?>

        <div class="container">

<?php $this->load->view($cuerpo_pagina); ?>

            <hr>

            <footer>
                <p><strong>Controlador:</strong> controllers/initializr.php <strong>Vista:</strong> views/initializr/vista_base.php</p>
            </footer>

        </div>

<?php $this->load->view('initializr/vista_pie_de_pagina.php'); ?>
