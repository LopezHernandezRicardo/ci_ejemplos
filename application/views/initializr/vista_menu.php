        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <?php echo anchor('inicio/index', 'Codeigniter/Initializr', 'class="brand"'); ?>
                    <div class="nav-collapse collapse">
                        <ul class="nav">
                            <li><?php echo anchor('inicio/index', 'Inicio'); ?></li>
                            <li class="active"><?php echo anchor('initializr/ingresar', 'Ingresar'); ?>"></li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Operación<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                <li><?php echo anchor('operacion/suma', 'Suma'); ?></li>
                                <li><?php echo anchor('operacion/resta', 'Resta'); ?></li>
                                <li><?php echo anchor('operacion/multiplicacion', 'Multiplicación'); ?></li>
                                <li><?php echo anchor('operacion/division', 'División'); ?></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Área<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                <li><?php echo anchor('operacion/cuadrado', 'Cuadrado'); ?></li>
                                <li><?php echo anchor('operacion/triangulo', 'Triangulo'); ?></li>
                                <li><?php echo anchor('operacion/circulo', 'Círculo'); ?></li>
                                </ul>
                            </li>
                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>
